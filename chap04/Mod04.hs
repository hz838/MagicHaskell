{-
    第4章：拉链Zip
-}

--通过一个Helper函数进行尾递归替代循环
--实现提取下标为5的倍数的元素的功能
giveMeFiveHelper :: Int -> [a] -> [a]
giveMeFiveHelper _ [] = []
giveMeFiveHelper i (x:xs) =
    if i `rem` 5 == 0 then x : giveMeFiveHelper (i+1) xs
                      else giveMeFiveHelper (i+1) xs

giveMeFiveA :: [a] -> [a]
giveMeFiveA xs = giveMeFiveHelper 0 xs

--假如原始数据是[(Int, a)]的元组类型，就可以直接用模式匹配了
giveMeFiveB :: [(Int, a)] -> [a]
giveMeFiveB [] = []
giveMeFiveB ((i,x):xs) =
    if i `rem` 5 == 0 then x : giveMeFiveB xs
                      else giveMeFiveB xs

{-
    拉链Zip的功能就是把[x1, x2, ...]转换成[(0,x1)...]
    例如运行giveMeFiveB (zip [0..][1..100])
-}
