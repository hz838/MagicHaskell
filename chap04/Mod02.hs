{-
    第4章：类型推断，相互矛盾的类型
-}

-- 本程序无法编译通过

module Test where

addOne :: Int -> Int
addOne x = x + 1

hello :: String -> String
hello x = "hello " + x

main = print (addOne (hello 2))