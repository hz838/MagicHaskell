# 第4章第6组

## 柯里化

```haskell
curry :: ((a, b) -> c) -> a -> b -> c

uncurry :: (a -> b -> c) -> (a, b) -> c
```

curry把一个`(a, b) -> c`类型的函数变成`a -> b -> c`类型的函数，uncurry反之。