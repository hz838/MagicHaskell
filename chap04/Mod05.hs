{-
    第4章：zipWith
-}

{-
    zipWith的类型：(a->b->c)->[a]->[b]->[c]
    运用a->b->c类型的函数，把[a]和[b]缝合成[c]
-}

zipWithAdd = zipWith (+) --高阶函数