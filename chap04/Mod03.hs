{-
    第4章：高阶函数
    高阶函数，即返回函数的函数
-}

replicateThree = replicate 3

-- 本函数类型为 a -> [a]，是一个返回函数的高阶函数