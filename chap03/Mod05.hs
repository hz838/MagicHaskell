{-
    递归案例：以下例子都是prelude定义的，
    不需要载入本文件。
-}

--计算列表长度
length :: [a] -> Int
length [] = 0
length (_:xs) + 1 + length xs

--取列表的起始部分（除掉最后一个元素）
init :: [a] -> [a]
init [] = error "empty list"
init [x] = []
init (x:xs) = x : init xs

--取列表的末（最后一个元素）
last :: [a] -> [a]
last [] = error "empty list"
last [x] = x
last (x:xs) = xs

--取列表的前n个元素
take :: Int -> [a] -> [a]
take 0 _ = []
take n (x:xs) = x : take (n-1) xs

--丢弃列表的前n个元素
drop :: Int -> [a] -> [a]
drop 0 xs = xs
drop n (_:xs) = drop (n-1) xs

--构造长度为n的列表
replicate :: Int -> a -> [a]
replicate 0 _ = []
replicate n x = x : replicate (n-1) x