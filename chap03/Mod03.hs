{-
    第3章。列表。
    列表的模式匹配
-}

listA = [1,2,3,4]

pmA [a,b,c,d] = a --运行pmA listA得到1

pmB (a:b:c) = c --运行pmB listA得到[3,4]

{-
    列表的右侧是[]列表的递归构造是a:[a] | []。
    通过以下例子可以清晰地看到对列表右侧进行这种模式匹配时会得到列表。
-}
pmC (a:b:c:d) = d --运行pmC listA得到[4]

pmD (a:b:c:d:e) = e --运行pmD listA得到[]

pmE (a:b:[c,d]) = c --运行pmE listA得到3