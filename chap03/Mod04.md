#第3章第4组

## `(!!)`函数
Haskell中没有`listA[2]`这样取元素的语法，而是采用`(!!)`函数。定义如下：

```haskell
(!!) :: [a] -> Int -> a
(x:xs) !! 0 = x
(x:xs) !! n = xs !! (n-1)
```

## 全函数与偏函数

上述程序如果下标太大会报错，因此称作**偏函数**，可以通过`Maybe`将其定义为**全函数**这样无论如何都不报错。

```haskell
(!!) :: [a] -> Int -> Maybe a

[] !! _ = Nothing
(x:xs) !! 0 = Just x
(x:xs) !! n = xs !! (n-1)
```
