{-
    第3章。列表。
    列表是递归构造的。
-}

-- 以下方法可以自行定义一个列表
data List a = a :< List a | Nil
-- 书中使用List a = a : a | Nil，但GHC中:指定为已有的列表使用，所以不能用这个constructor

-- Haskell等差数列创建列表

listA = [1..7]

listB = [1..]

listC = [1,3..7]

listD = [1,3..]

listE = [1,1..]

listF = [2,1..]

listG = [10,9..0] --注意9后面没有逗号