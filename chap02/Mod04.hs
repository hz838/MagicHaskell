{-
    关于P19的最后一条规则：不阻止左侧出现的类型变量不在右侧出现
-}

-- 需要参数的构造函数Just，无参数构造函数Nothing
data Maybe a = Just a | Nothing

{-
    Maybe可以看作一个盒子，里面有可能是Just xxx，也可能是Nothing
-}
