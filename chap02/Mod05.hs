{-
    P21记录语法record syntax
-}

data Position = MakePosition{
        posX :: Double
    ,   posY :: Double
}

pointA = MakePosition 3 4

-- 使用pointA和记录语法创造新的数据
-- pointB和pointA互相不会影响
pointB = pointA {posY = 5}

distance :: Position -> Position -> Double
distance (MakePosition x1 y1) (MakePosition x2 y2) =
    sqrt((x1 - x2) ^ 2 + (y1 - y2) ^ 2)