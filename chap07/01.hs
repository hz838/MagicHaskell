{-
    第7章：类型类
    实例声明：
    例如此前定义的data Position，无法运用==来判断相等，这是因为Position不是Eq的实例
    因此需要运用实例声明来解决这一问题
-}

data Position = Cartesian Double Double | Polar Double Double

instance Eq Position where
    Cartesian x1 y1 == Cartesian x2 y2 = (x1 == x2) && (y1 == y2)
    Polar x1 y1 == Polar x2 y2 = (x1 == x2) && (y1 == y2)
    Cartesian x y == Polar a r = (x == r * cos a) && (y == r * sin a)
    Polar a r == Cartesian x y = (x == r * cos a) && (y == r * sin a)

