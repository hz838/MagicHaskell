{-
    第5章：7-η转换
    实质：证明两个函数相等的唯一办法
    就是证明其在所有输入的情况下相等
-}

--以下两个函数是等价的
--nextCharA是point-free style
nextCharA :: Char -> Char
nextCharA = toEnum . (+1) . fromEnum

--nextCharB是point-full style
nextCharB :: Char -> Char
nextCharB c = toEnum . (+1) . fromEnum $ c
