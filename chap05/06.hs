{-# LANGUAGE MultiWayIf #-}
{-
    第5章-6：MultiWayIf
    需要在代码顶部加入一行来使用这个扩展
-}

isLarge x =
    if | x < 10 -> "too small"
       | x < 100 -> "middle"
       | x >= 100 -> "large"
