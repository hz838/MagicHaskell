{-
    第5章-1 应用函数。
    应用函数$和&。接收a->b类型的函数
    （因为b可以是任何类型所以实际上也可以是个函数，
    就是任意类型的函数）并计算f x
    $为右结合，&为左结合，可以替代括号用。
-}

--再写一个giveMeFive用于调用
giveMeFive :: [(Int, a)] -> [a]
giveMeFive []  = []
giveMeFive ((i,x):xs) =
    if i `rem` 5 == 0 then x : giveMeFive xs
                      else giveMeFive xs
--可以调用：giveMeFive $ zip [0..][1..100]

{-
    还有&在Data.Function中。从左往右
    如3 & (+1) & (2^)返回16
    (+1) 是 x + 1，这里x是3
    2^ 是 2 ^ x，x是3+1=4
-}