{-
    第5章-4：where
    用来书写函数内部用到的帮助函数。
-}

--还是giveMeFive这个例子
giveMeFive :: [a] -> [a]
giveMeFive xs = go $ zip [0..] xs
    where
        go [] = []
        go ((i, x):xs) = if i `rem` 5 == 0 then x : go xs
                                           else go xs
--这里go是帮助函数，如果单独写下来会很长，所以写成go再用where

--另外一种写法，再case，let里也可以用where
giveMeFiveB :: [a] -> [a]
giveMeFiveB xs = go $ zip [0..] xs
    where
        go [] = []
        go ((i, x):xs) =
            case i `rem` 5 of
                0 -> x : rest
                _ -> rest
                where
                    rest = go xs

{-
    这种写法说明在case里面也可以用where
-}