-- 模块声明
module Mod01 where

-- addOne
addOne :: Int -> Int
addOne x = x + 1

-- welcomeMsg
welcomeMsg :: String
welcomeMsg = "Hello, World!"
