{-
    第6章之折叠
    折叠：把一个列表聚合成一个值的操作。
-}

--用折叠实现一个map
--这个折叠折叠到了一个列表
newMap f xs = foldr ((:) .  f) [] xs

--用折叠实现length
lengthFR :: [a] -> Int
lengthFR = foldr (\_ acc -> acc + 1) 0 --注意foldr要求f是a->b->b

lengthFL :: [a] -> Int
lengthFL = foldl (\acc _ -> acc + 1) 0 --注意foldl要求f是b->a->b

