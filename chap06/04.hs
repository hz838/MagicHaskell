{-
    第6章之扫描。
-}

--3个数字的子串的最小字串和

minSubList :: (Num a, Ord a) => [a] -> Int -> a
minSubList xs m = initSum + minDiff
    where
        shifted = drop m xs
        initSum = sum $ take m xs
        minDiff = minimum $ scanl (+) 0 $ zipWith (-) shifted xs

{-
    优化方法：(initXs, shifted) = splitAt m xs
    initSum = sum initXs
-}