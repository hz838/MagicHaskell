{-
    第6章之过滤
-}

--使用filter制作GiveMeFive函数

giveMeFive :: [a] -> [a]
giveMeFive xs = map snd $ filter (\(i, x) -> i `rem` 5 == 0) $ zip [0..] xs

--注意snd是取元组第二个元素的意思

